Merge Requests
Descriptions
We can think of templates. We can decide on the content of this template
Size of MR
Smaller MRs. Solving a particular problem/feature
Lets avoid scope creep
Reviews
Review comments should be resolved by the reviewer
Should all the comments be resolved?  Yes, If it’s just a comment any one can resolve it
An MR can only be merged with one positive review. But an MR should have more than one reviewer when possible.Number of reviewers >= 1?  
Commits
One commit per MR? We should try for it, if not possible squash before merge.
Squash before Merge? We need to squash before merge and this can be done from the gitlab UI. Enable merge method (fast forward) and enable(require) for squash
Coverage
New line of code should have test coverage. It should not lower the coverage numbers. 
Documentation
Should have minimal documentation.(outside the code) . Mandatory for the new features.
Traceability from kanban to MRs and vice versa
Keep doing what we already have. We should not track the board from the MR
